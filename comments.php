<?php
require_once "top.php";
require_once "util.php";


if( isset( $_POST["YTapi"] ) && $_POST["YTapi"]!="" && $_POST["video_id"] && $_POST["video_id"]!=""  && $_POST["search_terms"]!="" ){
    $api_key = filter_var( (string)$_POST["YTapi"], FILTER_SANITIZE_STRING);
    $video_id = filter_var( (string)$_POST["video_id"], FILTER_SANITIZE_STRING);

    $search_terms = explode(';', filter_var(  (string)$_POST["search_terms"] , FILTER_SANITIZE_URL));	
    $comments = array();
    $results = array();

    foreach ($search_terms as &$search_term) {
        $comments_containing_this_search_term =  fetch_all_comments ($video_id,$api_key,$search_term);
        $comments = array_merge($comments, $comments_containing_this_search_term );
        $results[$search_term] = count( $comments_containing_this_search_term );
    }
}
else{
    echo "missing api key or video id or search terms";
}

?>
<div class="container">
    <div id="main">
        <h1>The results</h1>
        <?php
        foreach ($results as $key => $value){
            $percentage = round( ( $value / count($comments) )*100 , 2 );
            echo " Comments containing $key: $value ($percentage %) <br/>";
        }
        ?>
        <p>Total number of comments (containing any of the search terms): <?php echo count($comments); ?></p>

        <div>
        <h3>Comments containing search terms</h3>
        <table class="table table-striped">
        <tbody>
        <?php
            foreach ($comments as &$comment) {
                echo "<tr><td>${comment}</td></tr>";
            }
        ?>
        </tbody>
        </table>
        </div>
    </div>
</div>

</body>
</html>
