<?php

function fetch_all_comments (string $video_id, string $api_key, string $search_term){

    $comments = array();
    $nextToken = "";
    $lastNextToken = "";
    $loadNextPage = false;



    $cacheFile = getcwd()."/cache/${video_id}_${search_term}.json";

    //check if comments are cached
    if ( file_exists($cacheFile) ){
        $cached = file_get_contents($cacheFile);
        return json_decode($cached);
    }

        
    do {

        $cURLConnection = curl_init();

        if ($loadNextPage) { // we are not at last page
            curl_setopt($cURLConnection, CURLOPT_URL, "https://www.googleapis.com/youtube/v3/commentThreads?part=snippet&moderationStatus=published&order=time&maxResults=100&textFormat=plainText&searchTerms=${search_term}&videoId=${video_id}&key=${api_key}&pageToken=${nextToken}");
    
        }
        else { // last page of comments
            curl_setopt($cURLConnection, CURLOPT_URL, "https://www.googleapis.com/youtube/v3/commentThreads?part=snippet&moderationStatus=published&order=time&maxResults=100&textFormat=plainText&searchTerms=${search_term}&videoId=${video_id}&key=${api_key}");
        }
    
        curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($cURLConnection, CURLOPT_TIMEOUT,10);
    
    
        $response = curl_exec($cURLConnection);
        $httpcode = curl_getinfo($cURLConnection, CURLINFO_HTTP_CODE);
    
    
        $response_object = json_decode($response);
    
        if ( $httpcode !== 200){
            curl_close($cURLConnection);
                
            echo "<pre>";
            echo "Something went horribly wrong! <br/>";
            echo 'HTTP status code from YouTube API: ' . $httpcode ."<br/><br/>";
    
            echo "Response from YouTube API:";
            print_r($response_object);
            echo "</pre>";
            die();
        }
        
            

        if (! empty( $response_object->{"nextPageToken"})){
            $nextToken = $response_object->{"nextPageToken"};
        }
        
        
        //pick out the actual comments from response data
        foreach ($response_object->{"items"} as &$item) {
            $comment = $item->{"snippet"}->{"topLevelComment"}->{"snippet"}->{"textDisplay"};
            array_push($comments, $comment);
        }

        // check if there is next page of comments to load
        //if( ($nextToken !== "" && $lastNextToken !== $nextToken) && ($response_object->{"pageInfo"}->{"totalResults"} == $response_object->{"pageInfo"}->{"resultsPerPage"}) ){
        if( ($nextToken !== "" ) ){
            $loadNextPage = true;
        }else{
            $loadNextPage = false;
        }
    
        curl_close($cURLConnection);

    }while ( $loadNextPage );

    // cache comments if not already cached or cache too old (24h)
    if ( ! file_exists($cacheFile) || ( time()-filemtime($cacheFile) > 24 * 3600 ) ){
        file_put_contents($cacheFile,json_encode($comments));
    }
    

    return $comments;
}

function dd($var){
    die( var_dump($var) );
}

function containsWord($str, $word)
{
    return !!preg_match('#\\b' . preg_quote($word, '#') . '\\b#i', $str);
}