# baldermorts_youtube_comment_counter
A simple react page to help out Baldermort’s Guides to Warhammer (https://www.youtube.com/channel/UCdpGd0jls7VQh6LvcZT_p1A) to count youtube comments containing certain strings.

# Run on your local machine
Run the PHP web server `php -S localhost:8000`